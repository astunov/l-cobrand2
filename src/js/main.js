//= ../../bower_components/scrollreveal/dist/scrollreveal.min.js

// slider
(function(){

  var btn = document.querySelector('.js-btn');
  var btnContainer = document.querySelector('.js-slider__list');
  var slide = document.querySelector('.js-slider__thumb a');
  var slideContainer = document.querySelector('.js-slider__thumb');
  var ctaBtn = document.querySelector('#note-cta');

  var map = [
      {
        "title": "Выбрать универсальный ноутбук <br> с выгодой <strong>от 1 649 <i class=\"rub\">a</i></strong>",
        "link": "https://www.ulmart.ru/catalog/10098155",
      },
      {
        "title": "Выбрать легкий и компактный <br>ноутбук с выгодой <strong>от 1 349 <i class=\"rub\">a</i></strong>",
        "link": "https://www.ulmart.ru/catalog/10098194",
      },
      {
        "title": "Выбрать игровой ноутбук <br> с выгодой <strong>от 5 970 <i class=\"rub\">a</i></strong>",
        "link": "https://www.ulmart.ru/catalog/10098135",
      },
      {
        "title": "Выбрать ультрабук <br> с выгодой <strong>от 6 599 <i class=\"rub\">a</i></strong>",
        "link": "https://www.ulmart.ru/catalog/10098442",
      },
      {
        "title": "Выбрать ноутбук-трансформер <br> c выгодой <strong>от 1 799 <i class=\"rub\">a</i></strong>",
        "link": "https://www.ulmart.ru/catalog/10100742",
      },
      {
        "title": "Выбрать бюджетный ноутбук <br> c выгодой <strong>от 1 499 <i class=\"rub\">a</i></strong>",
        "link": "https://www.ulmart.ru/catalog/10098154",
      }
  ]

  addClass(btn, 'is-active');
  addClass(slide, 'is-active');

  function initSlider(el) {
    var activeItemOrder = el.getAttribute('data-thumb');
    var prevActiveEl = btnContainer.querySelector('.is-active');
    var prevActiveSlide = slideContainer.querySelector('.is-active');
    var activeSlide = slideContainer.querySelector('a:nth-child(' + activeItemOrder + ')');

    // set state of toggle  btn
    removeClass(prevActiveEl, 'is-active');
    addClass(el, 'is-active');

    // set state of slide
    removeClass(prevActiveSlide, 'is-active');
    addClass(activeSlide, 'is-active');

    // set state of cta btn
    ctaBtn.innerHTML = map[activeItemOrder - 1 ].title;
    ctaBtn.setAttribute('href', map[activeItemOrder - 1].link);
  }

  btnContainer.addEventListener('click', function(e) {
    var target = event.target;
    if (!hasClass(target, 'js-btn')) return;
    initSlider(target);
  });

})();

// Sticky menu
(function(){

  var menu = document.querySelector('.js-topbar');
  var initialOffsetEl = document.querySelector('.l-promo');
  var initialOffset = initialOffsetEl.offsetHeight;
  var scrollTimeout;

  function addStickyToHeader() {
    var scrollTop = window.pageYOffset || document.documentElement.scrollTop;

    if ((scrollTop > initialOffset) &&
      !(hasClass(menu, 'is-sticky'))
      )
    {
      removeClass(menu, 'is-hidden');
      addClass(menu, 'is-sticky');
    } else if (scrollTop <= initialOffset) {
      removeClass(menu, 'is-sticky');
      addClass(menu, 'is-hidden');
    }
  }

  addStickyToHeader();

  window.addEventListener('scroll', function(e){
    clearTimeout(scrollTimeout);
    scrollTimeout = setTimeout(addStickyToHeader, 100);
  });

})();

// animate

window.sr = ScrollReveal();
  sr.reveal(
      '.js-tile',
      {
        duration: 1000,
        opacity: 0.01,
        delay: 0,
        easing:'cubic-bezier(0.25, 0.1, 0.25, 1)',
        distance: '100px',
        scale: 1,
      });


// HELPER FUNCTIONS

/**
 * @element el
 */
function hasClass(el, className) {
  if (el.classList)
    return el.classList.contains(className);
  else
    return new RegExp('(^| )' + className + '( |$)', 'gi').test(el.className);
}

function removeClass(el, className) {
  if (el.classList)
    el.classList.remove(className);
  else
    el.className = el.className.replace(new RegExp('(^|\\b)' + className.split(' ').join('|') + '(\\b|$)', 'gi'), ' ');
}

function addClass(el, className) {
  if (el.classList)
    el.classList.add(className);
  else
    el.className += ' ' + className;
}




