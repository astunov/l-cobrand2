# Project start
* npm i OR
* npm link gulp gulp-sourcemaps browser-sync rimraf gulp-jade gulp-sass gulp-plumber gulp-autoprefixer gulp-rigger gulp-cssfont64 gulp-svg-sprite gulp-svgmin gulp-cheerio gulp-replace gulp-directory-sync gulp-uglify gulp-imagemin imagemin-pngquant gulp-csso
* bower i
* gulp
* http://localhost:9000/

---

# SCSS 

## The 7-1 Pattern
* base
* layout
* pages
* themes
* abstracts
* vendors
* components

## Prefixes

* l- Layout
* h- Helper
* c- Component
* o- Object — universal cross-project components
* is--, has State
* js- JavaScript binds

Exception: grid. Row\cols are without any prefixes.

---

# TODO


# ISSUES



